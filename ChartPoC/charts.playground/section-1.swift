// Playground - noun: a place where people can play

// Playground - noun: a place where people can play



import UIKit

import CoreText



// This is a simple class for handling charts







var chartBackgroundColor = 0





func drawAxises() {
    
    
    
}





// TODO: Implement increments, start, ranges



// So this is nice but I can't pass it in as a named parameter



struct GenericRange<T> {
    
    let upper:T
    
    let lower:T
    
    
    
}



struct RangeDouble {
    
    var upper:Double
    
    var lower:Double
    
}



struct myRect {
    
    var top:Double
    
    var bottom:Double
    
    var left:Double
    
    var right:Double
    
}



struct TickStyle{
    
    var font:NSString;
    
    var color:UIColor;
    
    var strokeColor:UIColor;
    
    var strokeWidth:Float;
    
}



func drawGridLines( context:CGContextRef , x:CGFloat, y:CGFloat, width:CGFloat, height:CGFloat ){
    
    
    
    let increment = width/10
    
    var gridlinesColor = getGridlineColor()
    
    for var y1:CGFloat = y; y1 <= height; y1 += 40 {
        
        y
        
        CGContextMoveToPoint(context, x, y1)
        
        CGContextAddLineToPoint(context, width, y1)
        
        
        
    }
    
    
    
    /*  for var x1:CGFloat = x; x1 <= width; x1 += 10  {
    
    
    
    CGContextMoveToPoint(context, x1, x)
    
    CGContextAddLineToPoint(context, x1,  height)
    
    
    
    } */
    
    
    
    CGContextSetStrokeColorWithColor(context, getGridlineColor())
    
    CGContextStrokePath(context)
    
}



func drawGridLinesWithRange( context:CGContextRef, bounds:myRect, range1:RangeDouble)
    
{
    
    // Might want this return increment, line count?
    
    let increment = calculatePriceIncrementSizeBasedOnRange( range1.lower, range1.upper)
    
    
    
    var distance = range1.upper - range1.lower
    
    var pixelDistance = bounds.top - bounds.bottom
    
    var numberLines = ceil( distance/increment )
    
    // I really hate the typecasting
    
    var distanceBetweenLines = ceil(Double(pixelDistance ) / Double(numberLines))
    
    
    
    
    
    
    
    //  let pixelIncrement = calculatePixelIncrement(inc
    
    let gridlinesColor = getGridlineColor()
    
    
    
    
    
    var priceValue = range1.lower; // This need to be rounded in someway
    
    
    
    for var y1:Double = bounds.bottom; y1 <= pixelDistance; y1 += distanceBetweenLines {
        
        
        
        
        
        CGContextMoveToPoint(context, bounds.left, y1)
        
        CGContextAddLineToPoint(context, bounds.right - bounds.left-20, y1)
        
        
        
        CGContextSetStrokeColorWithColor(context, gridlinesColor)
        
        CGContextSetFillColorWithColor(context, gridlinesColor)
        
        drawTextOnChart(context,CGRectMake(bounds.right - bounds.left-20, y1-10, 100, 20),priceValue.description);
        
        priceValue = increment + priceValue;
        
    }
    
    
    
    /*  for var x1:CGFloat = x; x1 <= width; x1 += 10  {
    
    
    
    CGContextMoveToPoint(context, x1, x)
    
    CGContextAddLineToPoint(context, x1,  height)
    
    
    
    } */
    
    
    
    CGContextSetStrokeColorWithColor(context, getGridlineColor())
    
    CGContextStrokePath(context)
    
    
    
}



func drawTextOnChart(context:CGContextRef, rect:CGRect, text:NSString){
    
    
    
    var path:CGMutablePathRef = CGPathCreateMutable();
    
    CGPathAddRect(path, nil, rect);
    
    var fontRef:CTFontRef = CTFontCreateWithName("ArialMT", 15.0, nil)
    
    
    
    var attrs:NSDictionary = [kCTFontAttributeName:fontRef,kCTForegroundColorAttributeName: UIColor.whiteColor()];
    
    
    
    var attrString:NSAttributedString = NSAttributedString(string: text, attributes:attrs );
    
    
    
    var frameSet:CTFramesetterRef = CTFramesetterCreateWithAttributedString(attrString);
    
    var frame:CTFrameRef = CTFramesetterCreateFrame(frameSet,
        
        CFRangeMake(0, attrString.length), path, nil);
    
    
    
    // Flip the coordinate system
    
    
    
    CTFrameDraw(frame, context);
    
    
    
    
    
}



// Review what this does and why?



func createDeviceGrayColor( w:CGFloat, a:CGFloat) -> CGColorRef
    
{
    
    
    
    var gray = CGColorSpaceCreateDeviceGray()
    
    let comp = [w,a]
    
    let color = CGColorCreate(gray, comp)
    
    CGColorSpaceRelease(gray)
    
    return color
    
}



func getGridlineColor()->CGColorRef
    
{
    
    let color = createDeviceRGBColor(0.0, 222.0, 0.0, 1)
    
    return color
    
}



func getSeriesColor()->CGColorRef
    
{
    
    let color = createDeviceRGBColor(222.0, 0, 0, 1)
    
    return color;
    
}



func createDeviceRGBColor( r:CGFloat, g:CGFloat, b:CGFloat, a:CGFloat) ->CGColorRef
    
{
    
    let rgb = CGColorSpaceCreateDeviceRGB()
    
    let comps = [r,g,b,a]
    
    let color = CGColorCreate(rgb, comps)
    
    CGColorSpaceRelease(rgb)
    
    return color
    
    
    
}





func calculateLowerUpperBounds( series:Double[] ) -> (upper:Double, lower:Double){
    
    
    
    var upper = 0.0
    
    var lower = 0.0
    
    series.sort { $0 < $1}
    
    lower = series[0]
    
    upper = series[series.count - 1]
    
    return( upper, lower)
    
}





func calculatePriceIncrementSizeBasedOnRange(lowerBound:Double, upperBound:Double) ->Double
    
{
    
    
    
    var increment = 0.0
    
    // how to calculate a logical number of increments
    
    var delta = upperBound - lowerBound
    
    // there is a minimum number of lines
    
    switch delta
        
        {
        
            case 0..1: increment = 0.1
                
            case 1..2: increment  = 0.2
                
            case 2..5: increment  = 0.5
                
            case 5..10: increment = 1.0
                
            case 10..20: increment = 2.0
                
            case 20..50: increment = 5.0 // This is not working for some reason
                
            default: increment = ceil(delta/10)
        
    }
    
    
    
    return increment
    
    
    
}



func calculateTimeIcrementSizeBasedOnRange(){
    
    
    
}



func calculateRangeFromData()
    
{
    
    // basicall go thru the whole set of data calcalate the min and max
    
}

func plotSeries(context:CGContextRef, line:Dictionary<Double, Double>)
    
{
    
    var color = getSeriesColor()
    var firstPoint = true
    
    
    for (x, y) in line {
        
       // Need to fix the point system currently not using the correct stuff
       // Need to identify the scale and the range calculate the actual pixels for the point
        
        if ( firstPoint ) {
            CGContextMoveToPoint(context,x * 20 + 10, y * 20)
            firstPoint = false
        } else {
            CGContextAddLineToPoint(context, x * 20 + 10, y * 20)
        }
        
        
    }
    
    
    
    
    CGContextSetStrokeColorWithColor(context, color)
    CGContextSetFillColorWithColor(context, color)

    CGContextStrokePath(context)
    
    
    
}

var xScale = 1.0
var yScale = 1.0
var xOffset = 0.0
var yOffset = 0.0

func convertPointToPixel( x:Double, y:Double ) -> ( Double, Double) {
    // The logic to convert a point to a pixel
    // 1. Determine scale for each access
    // 2. Determine the offsets
    
    var x1 = ( x * xScale ) + xOffset
    var y1 = ( y * yScale ) + yOffset
    
    
    return ( x1 , y1 )
}

func generateRandomSeries(size:Int) -> Dictionary<Double, Double>
    
{
    
    var result:Dictionary<Double, Double> = [0.0:0.0]
    
    for var i = 0; i < size; i++ {
        
        
        var r = drand48()
        result[Double(i)] = r * 10
    }
    
    return result
    
    
    
}


func initializeChart() ->CGContextRef{
    UIGraphicsBeginImageContextWithOptions(CGSize(width:600,height:400), true, 1 )
    
    let bgColor = createDeviceGrayColor(0.5, 1.0)
    
    let context = UIGraphicsGetCurrentContext()
    
    let rectangle = CGRectMake(1, 1, 600, 400 )
    
    
    // Arun to explain ;-)
    //Like many of the low level APIs, Core Text uses a Y-flipped coordinate system
    //Following code is to fix the content orientation
    CGContextSetFillColorWithColor(context, bgColor)
    
    CGContextFillRect(context,rectangle)
    
    CGContextSetTextMatrix(context, CGAffineTransformIdentity); // 2-1
    
    CGContextTranslateCTM(context, 0, rectangle.size.height); // 3-1
    
    CGContextScaleCTM(context, 1.0, -1.0)
    
    return context
 
}


var series:Dictionary<Int,Double>   = [1:25,2:30, 3:27.5, 4:50,5:10]

let xSeries = Array(series.keys)

var test = generateRandomSeries(10)

let result = calculateLowerUpperBounds(Array(test.values))

//let result2 = calculateLowerUpperBounds(series: Double[])

let incrementy = calculatePriceIncrementSizeBasedOnRange(result.lower,result.upper)

let context = initializeChart()



//drawGridLines(context, 40, 40, 550, 350)

drawGridLinesWithRange(context, myRect(top:400.0, bottom:10.0, left:20.0, right:600.0), RangeDouble(upper: result.upper, lower: result.lower))

plotSeries(context, test)

//testPlotSeries(context)





var image = UIGraphicsGetImageFromCurrentImageContext()





//let xRange = calculateLowerUpperBounds(xSeries)

