//
//  ViewController.swift
//  ChartPoC
//
//  Created by William Shea on 6/30/14.
//  Copyright (c) 2014 William Shea. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
                            
    override func viewDidLoad() {
        super.viewDidLoad()
        let width:CGFloat = self.view.frame.width;
        let height:CGFloat = self.view.frame.height;
        
        var rect:CGRect = CGRectMake(20, 20, width-40, height-40)
        
        var cb:ChartBase = ChartBase()
        cb.createChart(rect,series: generateRandomSeries(10))
        
        var imageView:UIImageView = UIImageView(frame: rect);
        imageView.image = UIGraphicsGetImageFromCurrentImageContext()
        self.view.addSubview(imageView)
    }

    func generateRandomSeries(size:Int) -> Dictionary<Double, Double>
        
    {
        
        var result:Dictionary<Double, Double> = [0.0:0.0]
        
        for var i = 0; i < size; i++ {
            
            
            var r = drand48()
            result[Double(i)] = r * 10
        }
        
        return result
        
        
        
    }
}

