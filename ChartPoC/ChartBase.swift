//
//  ChartBase.swift
//  ChartPoC
//
//  Created by arun venkatesh on 7/3/14.
//  Copyright (c) 2014 William Shea. All rights reserved.
//

import Foundation
import UIKit
import CoreText

class ChartBase{
    
    
    // This is a simple class for handling charts
    var chartBackgroundColor = 0
    var numberFormatter:NSNumberFormatter = NSNumberFormatter()
    
    
    // So this is nice but I can't pass it in as a named parameter
    struct GenericRange<T> {
        let upper:T
        let lower:T
    }
    
    
    
    struct RangeDouble {
        var upper:Double
        var lower:Double
    }
    
    
    
    struct myRect {
        var top:Double
        var bottom:Double
        var left:Double
        var right:Double
    }
    
    struct TickStyle{
        var font:NSString;
        var color:UIColor;
        var strokeColor:UIColor;
        var strokeWidth:Float;
    }

    
    //This is the method that application will interact with to create the chart
    //in the frame that is one of the input params.
    //Also passes the data input
    func createChart(frame:CGRect , series:Dictionary<Double,Double>) ->CGContextRef{
        numberFormatter.maximumFractionDigits = 2;
        var context =  initializeChart(frame)
        let result = calculateLowerUpperBounds(Array(series.values))
                
        let incrementy = calculatePriceIncrementSizeBasedOnRange(result.lower,upperBound: result.upper)
        
        
        drawGridLinesWithRange(context, bounds: myRect(top:Double(frame.height), bottom:10.0, left:20.0, right:Double(frame.width)), range1: RangeDouble(upper: result.upper, lower: result.lower))
        
        plotSeries(context, line: series)
        
        return context
        
    }
    
    func initializeChart(frame: CGRect) ->CGContextRef{
        UIGraphicsBeginImageContextWithOptions(CGSize(width:frame.width,height:frame.height), true, 1 )
        let bgColor = createDeviceGrayColor(0.5,a:1.0)
        
        let context = UIGraphicsGetCurrentContext()
        
        let rectangle = CGRectMake(1, 1, frame.width, frame.height )
        
        
        // Arun to explain ;-)
        //Like many of the low level APIs, Core Text uses a Y-flipped coordinate system
        //Following code is to fix the content orientation
        CGContextSetFillColorWithColor(context, bgColor)
        CGContextFillRect(context,rectangle)
        CGContextSetTextMatrix(context, CGAffineTransformIdentity); // 2-1
        CGContextTranslateCTM(context, 0, rectangle.size.height); // 3-1
        CGContextScaleCTM(context, 1.0, -1.0)
        
        return context
        
    }
    
    func createDeviceGrayColor( w:CGFloat, a:CGFloat) -> CGColorRef
        
    {
        
        
        
        var gray = CGColorSpaceCreateDeviceGray()
        
        let comp = [w,a]
        
        let color = CGColorCreate(gray, comp)
        
        CGColorSpaceRelease(gray)
        
        return color
        
    }
    
    func calculateLowerUpperBounds( series:Double[] ) -> (upper:Double, lower:Double){
        
        
        
        var upper = 0.0
        
        var lower = 0.0
        
        series.sort { $0 < $1}
        
        lower = series[0]
        
        upper = series[series.count - 1]
        
        return( upper, lower)
        
    }
    
    func calculatePriceIncrementSizeBasedOnRange(lowerBound:Double, upperBound:Double) ->Double
        
    {
        var increment = 0.0
        
        // how to calculate a logical number of increments
        var delta = upperBound - lowerBound
        
        // there is a minimum number of lines
        switch delta
        {
            case 0..1: increment = 0.1
            
            case 1..2: increment  = 0.2
            
            case 2..5: increment  = 0.5
            
            case 5..10: increment = 1.0
            
            case 10..20: increment = 2.0
            
            case 20..50: increment = 5.0 // This is not working for some reason
            
            default: increment = ceil(delta/10)
            
        }
        
        return increment
    }
    
    func drawGridLinesWithRange( context:CGContextRef, bounds:myRect, range1:RangeDouble)
        
    {
        
        // Might want this return increment, line count?
        
        let increment = calculatePriceIncrementSizeBasedOnRange( range1.lower, upperBound: range1.upper)
        var distance = range1.upper - range1.lower
        var pixelDistance = bounds.top - bounds.bottom
        var numberLines = ceil( distance/increment )
        
        // I really hate the typecasting
        var distanceBetweenLines = ceil(Double(pixelDistance ) / Double(numberLines))
      
        //  let pixelIncrement = calculatePixelIncrement(inc
        let gridlinesColor = getGridlineColor()
        
        var priceValue = range1.lower; // This need to be rounded in someway
        
        for var y1:Double = bounds.bottom; y1 <= pixelDistance; y1 += distanceBetweenLines {
            
            
            CGContextMoveToPoint(context,CGFloat(bounds.left), CGFloat(y1))
            CGContextAddLineToPoint(context, CGFloat(bounds.right - bounds.left-20), CGFloat(y1))
            CGContextSetStrokeColorWithColor(context, gridlinesColor)
            CGContextSetFillColorWithColor(context, gridlinesColor)
            drawTextOnChart(context,rect: CGRectMake( CGFloat(bounds.right - bounds.left-20), CGFloat(y1-10), 100, 20),text: numberFormatter.stringFromNumber(priceValue));
            priceValue = increment + priceValue;
            
        }
        
        
        /*  for var x1:CGFloat = x; x1 <= width; x1 += 10  {
        CGContextMoveToPoint(context, x1, x)
        CGContextAddLineToPoint(context, x1,  height)
        } */
        CGContextSetStrokeColorWithColor(context, getGridlineColor())
        CGContextStrokePath(context)
    }
    
    func drawTextOnChart(context:CGContextRef, rect:CGRect, text:NSString){
        var path:CGMutablePathRef = CGPathCreateMutable();
        CGPathAddRect(path, nil, rect);
        var fontRef:CTFontRef = CTFontCreateWithName("ArialMT", 15.0, nil)
        var attrs:NSDictionary = [kCTFontAttributeName:fontRef,kCTForegroundColorAttributeName: UIColor.whiteColor()];
        var attrString:NSAttributedString = NSAttributedString(string: text, attributes:attrs );
        var frameSet:CTFramesetterRef = CTFramesetterCreateWithAttributedString(attrString);
        var frame:CTFrameRef = CTFramesetterCreateFrame(frameSet,CFRangeMake(0, attrString.length), path, nil);
        CTFrameDraw(frame, context);
        
    }
    
    
    func plotSeries(context:CGContextRef, line:Dictionary<Double, Double>)
        
    {
        var color = getSeriesColor()
        var firstPoint = true
        for (x, y) in line {
            
            var xvalue:CGFloat = CGFloat(x * 20.0 + 10.0)
            var yValue:CGFloat = CGFloat(y * 20.0)

            // Need to fix the point system currently not using the correct stuff
            // Need to identify the scale and the range calculate the actual pixels for the point
            if ( firstPoint ) {
                CGContextMoveToPoint(context, xvalue, yValue)
                firstPoint = false
            } else {
                CGContextAddLineToPoint(context, xvalue, yValue)
            }
            
        }
        
        CGContextSetStrokeColorWithColor(context, color)
        CGContextSetFillColorWithColor(context, color)
        
        CGContextStrokePath(context)
    }
    
    func getGridlineColor()->CGColorRef
        
    {
        let color = createDeviceRGBColor(0.0, g: 222.0, b: 0.0, a: 1)
        return color
    }
    
    func getSeriesColor()->CGColorRef
        
    {
        let color = createDeviceRGBColor(222.0, g: 0, b: 0, a: 1)
        return color;
    }
    
    func createDeviceRGBColor( r:CGFloat, g:CGFloat, b:CGFloat, a:CGFloat) ->CGColorRef
        
    {
        let rgb = CGColorSpaceCreateDeviceRGB()
        let comps = [r,g,b,a]
        let color = CGColorCreate(rgb, comps)
        CGColorSpaceRelease(rgb)
        return color
    }

}